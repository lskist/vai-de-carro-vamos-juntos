package com.vaidecarrovamosjuntos;

import java.util.ArrayList;

import org.w3c.dom.Text;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class PathSelectionActivity extends Activity {
	Ride ride = new Ride();

	static int DEPARTURE = 1;
	static int ARRIVAL = 2;
	static int TIME = 3;
	static int DAYS = 4;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.selectpath);
		
		TextView displayHour = (TextView) findViewById(R.id.textView1);
		displayHour.setText(ride.getTime());
		Button pickTimeButton, pickDaysButton, chooseDepartureButton, chooseArrivalButton;
		pickTimeButton = (Button) findViewById(R.id.button1);
		pickTimeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stua
				showDialog(TIME);
				//Toast.makeText(MainActivity.this, "batata", Toast.LENGTH_LONG).show();
			}
		});
		pickDaysButton = (Button) findViewById(R.id.button2);
		pickDaysButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stua
				showDialog(DAYS);
				//Toast.makeText(PathSelectionAcitivity.this, "batata", Toast.LENGTH_LONG).show();
			}
		});
		
		chooseDepartureButton = (Button) findViewById(R.id.button3);
		chooseDepartureButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stua
				 Intent i = new Intent(PathSelectionActivity.this, MapActivity.class);
				 startActivityForResult(i,DEPARTURE);
				//Toast.makeText(PathSelectionAcitivity.this, "batata", Toast.LENGTH_LONG).show();
			}
		});
		
		chooseArrivalButton = (Button) findViewById(R.id.button4);
		chooseArrivalButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stua
				 Intent i = new Intent(PathSelectionActivity.this, MapActivity.class);
				 startActivityForResult(i,ARRIVAL);
				//Toast.makeText(PathSelectionAcitivity.this, "batata", Toast.LENGTH_LONG).show();
			}
		});
	}


	@Override
	@Deprecated
	protected Dialog onCreateDialog(int id){
		AlertDialog alerta = null;
		View v = null;
		AlertDialog.Builder builder = new Builder(this);
		if(id == TIME) 
		{
			builder.setTitle("Horario");
			//define a mensagem
			//builder.setMessage("Qualifique este software");

			//builder.SetContentView(R.layout.piNULLck_time);

			//cria o AlertDialog
			alerta = builder.create();
			v = alerta.getLayoutInflater().inflate(R.layout.pick_time, null);
			final TimePicker tp = (TimePicker)v.findViewById(R.id.timePicker1);
			alerta.setButton(AlertDialog.BUTTON_POSITIVE, "OK!", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					ride.hour = tp.getCurrentHour();
					ride.minute = tp.getCurrentMinute();				
				}
			});
		}
		if(id == DAYS)
		{
			builder.setTitle("Dias");

			//cria o AlertDialog
			alerta = builder.create();
			v = alerta.getLayoutInflater().inflate(R.layout.pick_days, null);
						
			final CheckBox checkBoxMonday = (CheckBox) v.findViewById(R.id.checkBox3);
			final CheckBox checkBoxTuesday = (CheckBox) v.findViewById(R.id.checkBox4);
			final CheckBox checkBoxWednesday = (CheckBox) v.findViewById(R.id.checkBox5);
			final CheckBox checkBoxThursday = (CheckBox) v.findViewById(R.id.checkBox6);
			final CheckBox checkBoxFriday = (CheckBox) v.findViewById(R.id.checkBox7);
			final CheckBox checkBoxSaturday = (CheckBox) v.findViewById(R.id.checkBox8);
			final CheckBox checkBoxSunday = (CheckBox) v.findViewById(R.id.checkBox9);

			final ArrayList<CheckBox> checkedDays = new ArrayList();
			checkedDays.add(checkBoxMonday);
			checkedDays.add(checkBoxTuesday);
			checkedDays.add(checkBoxWednesday);
			checkedDays.add(checkBoxThursday);
			checkedDays.add(checkBoxFriday);
			checkedDays.add(checkBoxSaturday);
			checkedDays.add(checkBoxSunday);
			
			final CheckBox checkBoxAll= (CheckBox) v.findViewById(R.id.checkBox1);
			checkBoxAll.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					for (CheckBox checkBox : checkedDays) {
						checkBox.setChecked(isChecked);
					}
				}
			});		
			
			alerta.setButton(AlertDialog.BUTTON_POSITIVE, "OK!", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					int i=0;
					for (CheckBox checkBox : checkedDays) {
						ride.days[i] = checkBox.isEnabled();
					}
				}
			});			
		}

		alerta.setView(v);
		//Exibe
		return alerta;	  
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		  if (requestCode == DEPARTURE) {

		     if(resultCode == RESULT_OK){      
		    	Toast.makeText(PathSelectionActivity.this,data.getStringExtra("result"), Toast.LENGTH_LONG).show();         
		     }
		     if (resultCode == RESULT_CANCELED) {    
		         //Write your code if there's no result
		     }
		  }
		  
		  if (requestCode == ARRIVAL) {

			     if(resultCode == RESULT_OK){      
				   	Toast.makeText(PathSelectionActivity.this,data.getStringExtra("result"), Toast.LENGTH_LONG).show();         
      
			     }
			     if (resultCode == RESULT_CANCELED) {    
			         //Write your code if there's no result
			     }
			  }
		}//onActivityResult

}

