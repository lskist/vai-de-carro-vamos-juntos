package com.vaidecarrovamosjuntos;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends Activity {
	
    //atributo da classe.

    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button carona, caroneiro;

		carona = (Button) findViewById(R.id.button1);
		carona.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stua
				// Passa atividade atual e atividade desejada como parametos. Eh o mais simples.
				Intent i = new Intent(MainActivity.this, MapActivity.class);
				startActivity(i);
				//Toast.makeText(MainActivity.this, "dsda", Toast.LENGTH_LONG).show();
			}
		});
		
		caroneiro = (Button) findViewById(R.id.button2);
		caroneiro.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stua
				// Passa atividade atual e atividade desejada como parametos. Eh o mais simples.
				Intent i = new Intent(MainActivity.this, PathSelectionActivity.class);
				startActivity(i);
				//Toast.makeText(MainActivity.this, "dsda", Toast.LENGTH_LONG).show();
			}
		});
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
