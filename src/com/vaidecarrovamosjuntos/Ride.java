package com.vaidecarrovamosjuntos;

import com.google.android.gms.maps.model.LatLng;

public class Ride {
	
	/* 
	 * Arquivo define uma carona entre dois pontos no mapa (Latitude/Longitude), com um horario de saida
	 * 	e uma lista de dias das semana que ocorrera.
	 * Mais tarde sera adicionado um id da pessoa que esta dando a carona e os ids de quem aceitou.
	 */
	
	LatLng destiny; 
	LatLng through;
	LatLng departure;
	int hour=-1;
	int minute=-1;
	boolean[] days = new boolean[7];
	public String getTime() {
		if(0 <= hour && 0 <= minute)
			return String.valueOf(this.hour)+':'+String.valueOf(this.minute);
		else
			return "--:--";
	}
	
}
